const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem4 = require('../problem4');

// expected result array
const expected = [
    2009, 2001, 2010, 1983, 1990, 1995,
    2009, 1987, 1996, 2000, 2004, 2004,
    1997, 1999, 2000, 2001, 1987, 1995,
    1994, 1985, 2003, 1997, 1992, 2003,
    2005, 2005, 2000, 2005, 1993, 2010,
    1964, 1999, 2011, 1991, 2000, 2003,
    1997, 1992, 1998, 2012, 1965, 1996,
    2009, 2012, 2008, 1995, 2007, 2008,
    1996, 1999
];

describe('Problem4', () => {
    it('Function should list all the car years', () => {
        assert.deepEqual(problem4(inventory), expected);
    });
    it('Function should return empty array when inventory is not passed', () => {
        assert.isArray(problem4());
        assert.isEmpty(problem4());
    });
    it('Return shouldn\'t be affected with more arguments', () => {
        assert.deepEqual(problem4(inventory, {}, 'Dumped string'), expected);
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem4({}));
        assert.isArray(problem4({ id: 33, name: "Test", length: 10 }));
        assert.isEmpty(problem4(new String("Mountblue")));
        assert.isArray(problem4(new String("mountblue")));
    });
});