const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem5 = require('../problem5');
const carYearslist = require('../problem4');
const carYears = carYearslist(inventory);

// expected result array
const expected = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
];

//testing problem5
describe('Problem5', () => {
    it('Function should list car years before 2000', () => {
        assert.deepEqual(problem5(inventory, carYears), expected);
    });
    it('Function should return empty array when inventory is not passed', () => {
        assert.isArray(problem5());
        assert.isEmpty(problem5());
    });
    it('Return shouldn\'t be affected with more arguments', () => {
        assert.deepEqual(problem5(inventory, carYears, {}, 'This is Bengaluru'), expected);
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem5({}));
        assert.isArray(problem5({ id: 1457651 }));
        assert.isEmpty(problem5(new String("MountblueTechnologies")));
        assert.isArray(problem5(new String("MountblueTechnologies")));
    });
    it('Function should return empty array when result from problem4 is empty', () => {
        assert.isEmpty(problem5(inventory, []));
        assert.isArray(problem5(inventory, []));
    });
});
