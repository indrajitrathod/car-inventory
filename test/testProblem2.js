const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem1 = require('../problem2');

describe('Problem2', () => {
    it('Function should return details of last car in the inventory ', () => {
        assert.deepEqual(problem1(inventory), { "id": 50, "car_make": "Lincoln", "car_model": "Town Car", "car_year": 1999 });
    });
    it('Function should return empty object when inventory is not passed', () => {
        assert.typeOf(problem1(), 'object');
        assert.isEmpty(problem1());
    });
    it('Return should be unaffected when extra arguments are passed', () => {
        assert.deepEqual(problem1(inventory, 84567, [], 'This is test'), { "id": 50, "car_make": "Lincoln", "car_model": "Town Car", "car_year": 1999 });
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem1({ id: 33, name: "Test", length: 10 }));
        assert.typeOf(problem1({ id: 33, name: "Test", length: 10 }), 'object');
        assert.isEmpty(problem1(new String("Mountblue")));
        assert.typeOf(problem1(new String("Mountblue")), 'object');

    });
    it('Function shouldn\'t throw error when inventory is empty array', () => {
        assert.isEmpty(problem1());
        assert.typeOf(problem1(), 'object');
    });
});