const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem6 = require('../problem6');

const expected = [
    {
        id: 6,
        car_make: 'Audi',
        car_model: 'riolet',
        car_year: 1995
    },
    {
        id: 8,
        car_make: 'Audi',
        car_model: '4000CS Quattro',
        car_year: 1987
    },
    {
        id: 25,
        car_make: 'BMW',
        car_model: '525',
        car_year: 2005
    },
    {
        id: 30,
        car_make: 'BMW',
        car_model: '6 Series',
        car_year: 2010
    },
    {
        id: 44,
        car_make: 'Audi',
        car_model: 'Q7',
        car_year: 2012
    },
    {
        id: 45,
        car_make: 'Audi',
        car_model: 'TT',
        car_year: 2008
    }
];

describe('Problem6', () => {
    it('Function should return array of objects which has only BMW or Audi Cars', () => {
        assert.deepEqual(problem6(inventory), expected);
    });
    it('Function should return empty array when inventory is not passed', () => {
        assert.isArray(problem6());
        assert.isEmpty(problem6());
    });
    it('Return shouldn\'t be affected with more arguments', () => {
        assert.deepEqual(problem6(inventory, {}, 'SDE FullStack'), expected);
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem6({}));
        assert.isArray(problem6({ id: 1457651 }));
        assert.isEmpty(problem6(new String("MountblueTechnologies")));
        assert.isArray(problem6(new String("MountblueTechnologies")));
    });
});