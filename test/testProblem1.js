const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem1 = require('../problem1');

describe('Problem1', () => {
    it('Function should return car object with id 33', () => {
        assert.deepEqual(problem1(inventory, 33), { "id": 33, "car_make": "Jeep", "car_model": "Wrangler", "car_year": 2011 });
    });
    it('Function should return empty array or object when id is not an number', () => {
        assert.isArray(problem1(inventory, 'Hello'));
        assert.isArray(problem1(inventory, []));
        assert.isEmpty(problem1(inventory, 'Hello'));
        assert.isEmpty(problem1(inventory, []));
    });
    it('Return should be empty when id is invalid', () => {
        assert.isEmpty(problem1(inventory, 84567));
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem1({ id: 33, name: "Test", length: 10 }, 33));
        assert.isArray(problem1({ id: 33, name: "Test", length: 10 }, 33));
        assert.isEmpty(problem1(new String("hello"), 33), 33);
        assert.isArray(problem1(new String("hello"), 33), 33);

    });
    it('Function shouldn\'t throw error when inventory data is not passed', () => {
        assert.isEmpty(problem1([], 33));
        assert.isArray(problem1([], 33));
        assert.isEmpty(problem1());
        assert.isArray(problem1());

    });
});