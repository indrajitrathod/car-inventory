const assert = require('chai').assert;
const { describe } = require('mocha');
const inventory = require('../cars');
const problem3 = require('../problem3');
const expected = [
    '300M',
    '4000CS Quattro',
    '525',
    '6 Series',
    'Accord',
    'Aerio',
    'Bravada',
    'Camry',
    'Cavalier',
    'Ciera',
    'Defender Ice Edition',
    'E-Class',
    'Econoline E250',
    'Escalade',
    'Escort',
    'Esprit',
    'Evora',
    'Express 1500',
    'Familia',
    'Fortwo',
    'G35',
    'GTO',
    'Galant',
    'Intrepid',
    'Jetta',
    'LSS',
    'MR2',
    'Magnum',
    'Miata MX-5',
    'Montero Sport',
    'Mustang',
    'Navigator',
    'Prizm',
    'Q',
    'Q7',
    'R-Class',
    'Ram Van 1500',
    'Ram Van 3500',
    'Sebring',
    'Skylark',
    'TT',
    'Talon',
    'Topaz',
    'Town Car',
    'Windstar',
    'Wrangler',
    'Wrangler',
    'XC70',
    'Yukon',
    'riolet'
];

describe('Problem3', () => {
    it('Function should list car models in alphabetical order', () => {
        assert.deepEqual(problem3(inventory), expected);
    });
    it('Function should return empty array when inventory is not passed', () => {
        assert.isArray(problem3());
        assert.isEmpty(problem3());
    });
    it('Return shouldn\'t be affected with more arguments', () => {
        assert.deepEqual(problem3(inventory, 5478415, 'Dumped string'), expected);
    });
    it('Function shouldn\'t throw error when inventory data is not an array', () => {
        assert.isEmpty(problem3({ id: 33, name: "Test", length: 10 }));
        assert.isArray(problem3({ id: 33, name: "Test", length: 10 }));
        assert.isEmpty(problem3(new String("Mountblue")));
        assert.isArray(problem3(new String("mountblue")));
    });
});

