// import the inventory data from the file cars.js
const inventory = require('./cars');

// function which iterate through the inventory and find a car details by its id
const carInfoById = (inventory, id) => {
    let ans = [];
    if (Array.isArray(inventory) && inventory.length && typeof id === 'number') {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id === id) {
                ans = inventory[i];
            }
        }
    }
    return ans;
}

module.exports = carInfoById;