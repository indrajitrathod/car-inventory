// import the inventory data from the file cars.js
const inventory = require('./cars');

// Function returns the last car info
const lastCarInfo = (inventory) => {
    let ans = {};
    if (Array.isArray(inventory) && inventory.length) {
        let lastCarIndex = inventory.length - 1;
        ans = inventory[lastCarIndex];
    }
    return ans;
}

module.exports = lastCarInfo;